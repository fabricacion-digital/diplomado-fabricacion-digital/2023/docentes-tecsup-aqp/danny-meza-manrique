# 4. Computer controlled cutting



## Using Plotter Cutting Machine

"To this assignment I am going to use a 2D design software (corel draw), plotter cutting machine (Roland GX-24) and cut software (cutStudio)"



## Starting with a design in Corel
"I select the image to the project"


- ![](../images/ChasperInkscape.jpg)

## Vectoring with

- ![](../images/ChasperInkscape.jpg)



### Using the plotter cutting machine

It works like a printer, it is necesary install a driver to cut.
1.- Feeding adhesive vinyl and initial settings.

2.- Before cut something, it is necesary set the size of adhesive vinil. (there must not be any design uploaded in Cut Studio)

3.- Now to import the design into CutStudio fron corel, i saved the design in .eps format.

You can download the design here:
Black; 
White; 
Red; 
Lightblue; 
4.- You can import this file into Studio cut and cut it.

5.- Finally your cutting plotter machine start to cut.

6.- I'm preparing a piece of acrylic to paste the image.
Bending the acrylic

- ![](../images/cortado.jpg)
- ![](../images/dobladoa.jpg)
- ![](../images/doblado.jpg)

## Ventilation system: to extract the smoke produced by cutting process
- ![](../images/ventilacion.jpg)


</p>
</div>
