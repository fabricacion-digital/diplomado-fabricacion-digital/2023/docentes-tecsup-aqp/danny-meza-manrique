# 3. Computer Aided design

Now I am trying some 2D and 3D design softwares.

## Trying Corel (2D design software)

It is a powerful tool to 2D design.

This is the design.

![](../images/2d.jpg)

## Trying Inkscape (2D design software)

Inksacape is a free 2D design software, it has many tools like corel.
This is an image about Inkscape.

![](../images/inkscape.jpg)

# 4. Trying Fusion 360 (3D design software)

Fusion 360 is a complete tool to 3D designs, it has many tool to create, edit, animate, process to several cnc machines.
Trying fusion 360