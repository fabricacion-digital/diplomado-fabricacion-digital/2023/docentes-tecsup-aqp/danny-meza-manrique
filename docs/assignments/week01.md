
# 1. Principles and practices

This week I worked on defining my final project idea and started to getting used to the documentation process.

## Research

"The research was carried out in a real environment, using a field design with a descriptive approach. Methods such as observation, through an assessment guide, were employed to diagnose the current state of logical-mathematical thinking skills in children. Additionally, a questionnaire was administered to the teacher to identify the games they use to develop this ability."

> "The results demonstrate that interactive games are a crucial tool to promote the development of these cognitive skills in students. These games not only help in solving learning problems but also improve their motor and cognitive abilities, and foster creativity, among other benefits.

It is important to highlight that, while interactive games are fundamental, they should be combined with other teaching strategies in the classroom. This approach provides children with a variety of opportunities to learn from their own experiences, enriching their educational process and fostering comprehensive learning.

In conclusion, interactive games are a valuable educational tool that significantly contributes to the development of logical-mathematical thinking in children."

## Useful links

- [Google](http://repositorio.unsa.edu.pe/handle/UNSA/8295)


## Purpose of the final project

Purpose of the final project.



![](../images/Esquema.jpeg)

## What will you do?


We will work with an application on a mobile device, communicating with the bluetooth receiver which is connected to the power stage, we will have different stages depending on the motor to be controlled and the casing for universal coupling of said motors.


## Who will use it?
Students of various technical careers, to understand the operation of actuators such as servomotors, flex sensors, microcontrollers and wireless communications, as well as the power stage in which we have transistors.