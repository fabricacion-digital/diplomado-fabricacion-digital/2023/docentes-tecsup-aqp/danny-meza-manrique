# 12. Output devices (Tarea 10)

# Documents how you determined the power consumption of an output device

- ![](../images/salidas-01.jpg)

# Document what you learned by connecting the output devices to the microcontroller and controlling the devices.

For this project, a servomotor was connected to the ESP32 microcontroller and the software was programmed to control its position programmatically.
The physical connection consisted of:
Connect the servomotor signal cable to a GPIO port configured as a digital output. In this case to GPIO 19.
Connect the positive cable of the servomotor to the +5V power line.
Connect the negative cable to the ground line or GND.
The following steps were carried out in the software:
Include the Servo library to control servo motors using the PWM protocol.
Declare a Servo object bound to GPIO 19 used for the signal.
In setup() initialize the communication with the servo using the attach() method.
In loop(), use the library's write() method passing values from 0 to 180 degrees to rotate the servomotor axis.
In this way, through small test programs, it was possible to establish the connection and programmatically control the position of the servomotor through the ESP32 microcontroller.


## Document your design and manufacturing process or link it to the board you made in the task

1. PCB Layout
For this project, the PCB board that was designed in the previous task was used, with the ESP32 already located on it.
An additional connector (J2) was included to connect the servomotor externally.
GPIO 19 of the ESP32 was assigned to a pin of connector J2, via a track on the PCB, to carry the control PWM signal.
2. PCB manufacturing
The PCB layout including the additional J2 connector was printed using the photolithography process.
Then the oiling, developing and immersion process was carried out to obtain the final plate.
3. Assembly of components
The ESP32 and the other components of the original board were assembled.
The J2 female connector was also installed in its corresponding place on the PCB.
4. Functional tests
The signal cable of a male servo motor was connected to connector J2.
The power cables to the voltage regulator.
The ESP32 was programmed to generate PWM signals and control the position of the servo.
This validated the correct functioning of the link between the PCB and the external servomotor.
In this way, the original design was adapted to include the control functionality of an external servomotor.


- ![](../images/salidas-02.jpg)


## programming processes that were used


The GPIO 19 pin was configured as a digital output to send the PWM signals, the Servo library was included to facilitate the control of the servomotor, a Servo type object linked to said pin was declared, and in the setup() the communication with the servo using the library's attach() method. Then, in the loop(), the write() method was used passing values between 0 and 180 degrees to generate the necessary PWM signals and rotate the servomotor shaft to different positions sequentially; In addition, delay() was included among the write() commands to allow the servomotor to complete each movement before receiving the next order, thus achieving full control of the servomotor to be programmed autonomously and sequentially through the code executed in the ESP32.


- ![](../images/salidas-03.jpg)



## Problems you encountered and how you solved them.

The only problem that was found was when transferring the programming to the esp32, it had complications since it needed to download a specific library.
When downloading the specific library, the program was able to pass satisfactorily.


## This criterion depends on a learning competency. Uploaded the files of your designs and programming codes.

- ![](../images/salidas-04.jpg)

## Main Image of the Activity

- ![](../images/salidas-05.jpg)

