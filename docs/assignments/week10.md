# 10. Electronic Design (Tarea 09 - 1)

Link to group assignments page.

https://www.flux.ai/dannym/placa-universal

## Documents what has been learned regarding the use of software for the design of electronic circuit boards.

What is flux.ai

Flux AI is a way to build electronic hardware and design PCBs faster and more efficiently with an AI assistance called Flux Copilot. Flux AI is a browser-based and AI-powered tool that helps you design electronics from idea to prototype.


Flux AI is a free tool ( Upgrade available ) for students, professionals, as well as electronics hobbyists, and enthusiasts. It makes the design process easier, faster, smarter, and more efficient for hardware engineers and PCB designers. 


Flux AI tool includes every feature needed to design electronic PCBs such as a component library, schematic and layout editor, simulation, etc. Also, you'll meet an AI teammate who will help you during the design process.


- ![](../images/flux-ia-01.jpg)


Why Do You Need Flux AI?

Let's discuss why you need Flux AI and why you should integrate it into your workflow. Here are some important reasons that will encourage you to use Flux AI:

Speed and Efficiency: Flux AI will help you enhance speed and efficiency during the design process by reducing repetitive tasks, never starting from scratch, using reusable designs, and keeping your team in synchronization.

Error Reduction: A powerful capability of Flux Copilot AI will help you to find and correct design errors and improve overall functionality.

Design Optimization: It will help you to optimize the overall schematic and PCB layout design. Also, the Flux Copilot will recommend helpful suggestions and give you optimization techniques.

Cost Savings: Flux AI is absolutely free for students, professionals, and electronics hobbyists. It provides enough features for designing PCBs in the free version. But for more, the pricing plans are also available which is good.

Complex Designs: Flux AI can help you solve complex circuit design problems and suggests tips, and methods to improve the overall design.

Automation: AI can automate various tasks in the design process which will reduce production time.

Collaboration: You can connect and collaborate with others and start your design journey with them in Flux. You can easily do remote work with collaborators and other team members here.

## step by step project development

1 ) Getting Started with Flux AI

Start with creating the account on Flux AI. Sign up or log in from the official website of Flux AI. Enter your name, email, and password to create the brand-new user account. Or you can continue with Google, and a GitHub account ( If have one ).


Getting started with Flux AI is free and doesn't require any additional downloads to use it. It is a browser-based tool that will run on your browser such as Chrome, Bing, Firefox, etc. 


After creating the account or login on the Flux AI website, it will provide you a dashboard where you can manage all of your things such as accounts, projects, components, community, etc. 

2 ) Setting Up Your Project

The next step is to create a new project. Start with creating a new project by clicking on the option "New Project" on your main dashboard. It will create a fresh project for you where you will work. 


After successfully creating a project, Flux will show you many things in the workplace:

Circuit or Schematic Design Editor: Here is where you will design your circuit or schematic by connecting various components through wires.

Code Editor: It is used to make powerful part automation, simulation purposes, and create custom components by writing code.

PCB Editor: It is a place where you will design your PCB layout and perform various tasks such as component placement, routing, etc.

Electronic Component Library ( Left Side ): It is an electronic component library provided by Flux where you can find, explore, and use various components from different manufacturers with different specifications.

Objects Menu: It will show you the components and their designators or part names you used in your project.

Inspector Menu: The Inspector menu is used to manage controls, and component properties, check component pricing, availability, and assets, and perform simulation.

Flux Copilot AI Chat Menu ( Right Side ): It is a chat menu where you can have conversations with Flux Copilot AI, ask questions, get suggestions, solve circuit and mathematical problems, etc. Start by writing "@Copilot" to give a prompt or send a message to AI.

- ![](../images/flux-ia-02.jpg)

3 ) Component Selection

Now you are ready to start your project. Here, we'll design the circuit and PCB layout of the LED Lightning Circuit for only learning purposes. Come to your circuit or schematic editor and start finding components from the library ( Left side ).


Find components for your project by searching keyword, name, or part number. You can also use filters to find components easily. It has a filter options such as,

Components with Footprints

Components with Layouts

Components with Simulation Models

Components with 3D Models

Components with Datasheets

Components with Manufacturer Part Numbers, etc.

Selecting the right component for electronics design is a very important step that you should consider during the project. Once you find out, just drag and drop that component into the blank space of the editor. 


- ![](../images/flux-ia-02.jpg)


4 ) Schematic Design

After component selection, it is time to design a schematic. Start by connecting the components using wires according to the circuit. 


Just connect the DC power supply to a resistor, the resistor to an LED, and make other Ground connections.


5 ) Using Flux Copilot AI

Flux Copilot can speed up your design process by integrating advanced AI algorithms that will analyze your design, and help you a lot to create an efficient electronic hardware system. 


Flux Copilot AI can help you in,

Electronic Hardware Design

PCB ( Printed Circuit Board ) Design

Circuit Layout

Component Selection

Schematic Design

Solving Complex Problems

Get Design Recommendation

Making Efficient Circuit Connections

Design Optimization

 Prototyping, etc.

Here, we just use Flux Copilot AI to describe everything about our project "LED Lightning".


6 ) PCB Layout Design

After circuit design, the next step is to design a PCB layout for that. Flux AI will automatically convert or update your schematic into PCB format. You just have to place the components and wire or route them using traces.


There are many more important aspects of PCB design that you should consider such as size, shape, PCB layer management, design rules, manufacturing capabilities, etc. 


Here, we only placed our components and routed them using traces in the top layer. Check out the 2D and 3D view of the designed PCB with Flux AI:
- ![](../images/flux-ia-03.jpg)


2D View of the PCB:

- ![](../images/flux-ia-04.jpg)

3D View of the PCB:
- ![](../images/flux-ia-05.jpg)



We export the PCB for milling

- ![](../images/flux-ia-06.jpg)

## file of its source code.

https://www.flux.ai/dannym/charming-salmon-replicator-8whn?editor=pcb_3d

## Activity Main Image
- ![](../images/flux-ia-07.jpg)