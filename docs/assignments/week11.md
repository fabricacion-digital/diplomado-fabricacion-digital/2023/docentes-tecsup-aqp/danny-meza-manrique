# 11. Electronic Production (Tarea 09 - 2)

Use of software for the design of electronic circuit boards.

The VPanel controller of the SRM-20 provides a simple interface to adjust the tool position and move the cursor to define the starting point of the milling operation. VPanel also allows easy control of feed rate and rotor speed with the ability to pause and resume operation, as well as milling tracking on the X, Y, Z axes with a numerical readout in millimeters or inches.

The SRM-20 in education
The SRM-20 is perfect for any classroom thanks to its compact design. Its ease of use means that extensive prior knowledge of the equipment is not necessary before its first use. The SRM-20 allows students to design and create complex concepts, elements and scale models. At the same time, it represents a very valuable option for teachers, since they will be able to have advanced prototyping technology in the classrooms.



- ![](../images/flux-ia-06.jpg)
- ![](../images/flux-ia-07.jpg)

## Plate development on THE SRM-20 MONOFAB MILLING MACHINE

- ![](../images/monofab01.jpg)

We download the FlatCAM program

https://bitbucket.org/jpcgt/flatcam/downloads/


- ![](../images/monofab02.jpg)

We open the FLAT CAM

- ![](../images/monofab03.jpg)


We open the saved flx ai program with the FLAT CAM software

- ![](../images/monofab04.jpg)
- ![](../images/monofab05.jpg)
- ![](../images/monofab06.jpg)
- ![](../images/monofab08.jpg)
- ![](../images/monofab09.jpg)

WE OPEN AND CONFIGURE THE FRAME

- ![](../images/monofab07.jpg)
- ![](../images/monofab10.jpg)


skinny cut

We place the plate in the SRM-20 MONOFAB MILLING MACHINE
- ![](../images/monofab11.jpg)

we configure the SRM-20 MONOFAB MILLING MACHINE for cutting
- ![](../images/monofab12.jpg)


We monitor the cutting of the SRM-20 MONOFAB MILLING MACHINE

- ![](../images/monofab13.jpg)
- ![](../images/monofab14.jpg)

Finished plate MILLING MACHINE SRM-20 MONOFAB
- ![](../images/monofab15.jpg)

soldered components on the board

- ![](../images/monofab16.jpg)


Problems presented and considerations

Milling of plates is a machining process used to shape, cut, or engrave materials such as metals, plastics, or wood, using a cutting tool called a milling cutter. Here are some important considerations to keep in mind when performing plate milling:

    Plate Material:
        Different materials require specific cutting speeds and feeds. Adjust milling parameters according to the material of the plate you are machining.

    Cutter Type:
        Select the appropriate cutter for the task. Cutters vary in shape, size, and material, and each is suitable for specific applications. For example, flat end mills are ideal for flat surfaces, while ball end mills are useful for three-dimensional contours.

    Cutting Speed and Feed:
        Adjust the cutting speed and feed rate according to the cutter manufacturer's recommendations and the material properties. Too high or too low cutting speed can affect the finish quality and tool lifespan.

    Depth of Cut:
        Determine the optimal depth of cut to avoid overloading the cutter and ensure efficient cutting. You can make cuts in multiple passes to prevent excessive tool stresses and improve finish quality.

    Plate Fixation:
        Ensure that the plate is securely fastened to the work area. Secure mounting prevents vibrations and ensures accurate and consistent results.

    Clamping System:
        Use appropriate clamping systems to hold the plate in place during milling. You can choose from vises, clamps, or other devices depending on the shape and size of the plate.

    Cooling and Lubrication:
        In milling, especially with metals, it's important to keep the temperature under control to avoid tool overheating. Use suitable cooling or lubrication systems according to the material's needs.

    CNC Programming (if applicable):
        If using a Computer Numerical Control (CNC) system, ensure correct programming of tool paths and adjustment of milling parameters in the G-code.

    Safety:
        Use personal protective equipment, such as safety glasses and hearing protection. Also, follow specific machine safety standards.

    Preliminary Testing:
        Before completing the milling process, conduct preliminary tests on scrap materials or non-visible areas to adjust parameters and ensure the final work's quality.

archive your designs and programming codes

https://www.flux.ai/dannym/charming-salmon-replicator-8whn?editor=pcb_2d


