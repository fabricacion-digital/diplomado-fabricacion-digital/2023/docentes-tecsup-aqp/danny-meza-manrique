# 9. Embedded programming (Tarea 08)

Evidence of the development of integrated programming

## The ESP32 

"The ESP32 is an advanced and highly versatile microcontroller that has gained popularity in the field of embedded systems development and the Internet of Things (IoT). Here are some of the key features of the ESP32:

    Dual Core: The ESP32 has two Xtensa LX6 processor cores, allowing for efficient multitasking.

    Adjustable Clock Speed: The ESP32 offers an adjustable clock speed, enabling developers to balance performance and power consumption according to their needs.

    Wireless Connectivity: It comes with integrated support for Wi-Fi and Bluetooth, facilitating connection to local networks and wireless communication between devices.

    Low-Energy Bluetooth (BLE) Capabilities: It includes support for BLE, making it ideal for low-power applications such as wearables and sensors.

    Wide Array of Peripherals: The ESP32 features a variety of peripherals, including SPI, I2C, UART, GPIO, ADC, and more, making it easy to connect to various devices and sensors.

    Flash Memory and RAM: It provides significant amounts of Flash memory and RAM, enabling the storage of programs and handling large datasets.

    Electromagnetic Interference (EMI) Protection: Incorporates EMI filters to reduce electromagnetic interference, enhancing the stability of wireless communications.

    Low Power in Idle Mode: It is energy-efficient, with low-power modes ideal for battery-powered applications.

    Support for Development in Arduino and Espressif IDF: You can program the ESP32 using the Arduino IDE or the Espressif IDF software development kit.

    Open Development Environment: It is an open-source microcontroller with an active community, providing a wide range of resources and libraries for development.

    Affordable Price: The ESP32 is known for being affordable, making it accessible for hobby projects and commercial applications."



- ![](../images/esp32.jpg)
- ![](../images/Arduino1.jpg)
- ![](../images/compareesppic.jpg)

## Programming your ESP32 development board to interact and communicate.

  we install the arduino softw
    - ![](../images/arduino2.jpg)
    Fill in the “Additional Board Manager URLs” field with the following.
    - ![](../images/arduino3.jpg)
    - ![](../images/arduino4.jpg)
    - ![](../images/arduino5.jpg)
    - ![](../images/arduino6.jpg)
  we select the processor, the port and download the program
    - ![](../images/arduino7.jpg)
    - ![](../images/arduino8.jpg)
    - ![](../images/arduino9.jpg)

  Programming processes

    Check the program



  - ![](../images/arduino10.jpg)

  Don't forget to select the processor and choose the communication port
  download the program
    - ![](../images/arduino11.jpg)
## Code Example

  - ![](../images/arduino13.jpg)
// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  // print out the value you read:
  Serial.println(sensorValue);
  delay(1);  // delay in between reads for stability
}

## main image of the activity

 
  - ![](../images/arduino12.jpg)